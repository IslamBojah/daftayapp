package com.orcas.orcas_islam.daftary_test1;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class LogActivity extends AppCompatActivity {
    ImageView im;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);


        im=(ImageView)findViewById(R.id.imageLog);

        final Animation myanim= AnimationUtils.loadAnimation(this,R.anim.mytransition);

        im.startAnimation(myanim);

        final Intent i=new Intent(this, Semester.class);

        Thread timer =new Thread(){
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            public void run(){
                try {
                    sleep(3000);
                }catch (InterruptedException e){

                    e.printStackTrace();
                }
                finally {
                    startActivity(i);
                    finish();
                }
            }
        };
        timer.start();

    }
}
