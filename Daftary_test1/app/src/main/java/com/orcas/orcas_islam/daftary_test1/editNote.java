package com.orcas.orcas_islam.daftary_test1;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;

public class editNote extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    SemesterDBHelper myDB;
    EditText note_title , note_body ;
    Button edit ;
    RecyclerView RcEdit;
    ImagesAdapter ImgAdapter;
    String id_note,title ,body,semid,mat_id;
    ArrayList<String> ImageIDS;
    private static final int IMAGES_LOADER = 0;
    Button addImage;
    android.support.v7.widget.Toolbar toolbarContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);

        Intent i = getIntent();
         id_note = i.getStringExtra("Note_id");
         title = i.getStringExtra("Note_title");
        body = i.getStringExtra("Note_Body");
         semid = i.getStringExtra("sem_id");
         mat_id = i.getStringExtra("mat_id");


        toolbarContent =(android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbarContent);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarContent.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        toolbarContent.setTitleMarginStart(650);


        // Toast.makeText(getApplicationContext(),id_note,Toast.LENGTH_SHORT).show();

        myDB = new SemesterDBHelper(this);
        ImageIDS =new ArrayList<String>();

        note_title =(EditText)findViewById(R.id.editNote_title);
        note_body =(EditText)findViewById(R.id.editNote_text);
        edit =(Button)findViewById(R.id.editNote_btn);
        addImage =(Button)findViewById(R.id.addImage_edit);

        note_title.setText(title);
        note_body.setText(body);



        Cursor c =myDB.getImage(id_note);

        if(c.getCount() !=0){

            while(c.moveToNext()){


                String y = c.getString(0);
                ImageIDS.add(y);
               // Toast.makeText(getApplicationContext(),y+"we",Toast.LENGTH_SHORT).show();
            }
        }

        note_title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Toast.makeText(getApplicationContext(),"welcome",Toast.LENGTH_SHORT).show();
                ImgAdapter = new ImagesAdapter(getApplicationContext(),ImageIDS ,id_note ,note_title.getText().toString(),note_body.getText().toString(),semid ,mat_id,false);
                RcEdit.setAdapter(ImgAdapter);
                getLoaderManager().initLoader(IMAGES_LOADER, null, editNote.this);

            }
        });


        note_body.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Toast.makeText(getApplicationContext(),"welcome",Toast.LENGTH_SHORT).show();
                ImgAdapter = new ImagesAdapter(getApplicationContext(),ImageIDS ,id_note ,note_title.getText().toString(),note_body.getText().toString(),semid ,mat_id,false);
                RcEdit.setAdapter(ImgAdapter);
                getLoaderManager().initLoader(IMAGES_LOADER, null, editNote.this);

            }
        });



        RcEdit = (RecyclerView) findViewById(R.id.imagesEdit);


        // mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        RcEdit.setLayoutManager(horizontalLayoutManagaer);

         ImgAdapter = new ImagesAdapter(this,ImageIDS ,id_note ,note_title.getText().toString(),note_body.getText().toString(),semid ,mat_id,false);
        RcEdit.setAdapter(ImgAdapter);

        getLoaderManager().initLoader(IMAGES_LOADER, null, this);
        /*
        RcEdit.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
              //  Toast.makeText(getApplicationContext(),"good buy"+rv.getId(),Toast.LENGTH_SHORT).show();

                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

*/

        //Toast.makeText(getApplicationContext(),id_note +title+body +semid +mat_id , Toast.LENGTH_SHORT).show();


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              boolean b=  myDB.updateNote(id_note , note_title.getText().toString() ,note_body.getText().toString(),semid ,mat_id);
                Intent m= new Intent(getApplicationContext() , NoteContent.class);
                m.putExtra("sem_id",semid);
                m.putExtra("mat_id", mat_id);

              if (b==true){
                    Toast.makeText(getApplicationContext(),"Data is update" , Toast.LENGTH_SHORT).show();

              }
                startActivity(m);
            }
        });


        addImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),AddnewImage.class);
                i.putExtra("sem_id",semid);
                i.putExtra("mat_id",mat_id);
                i.putExtra("Note_title",title);
                i.putExtra("Note_Body",body);
                i.putExtra("note_id",id_note);
                i.putExtra("value",false);
                startActivity(i);


            }
        });



    }
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                SemesterDBHelper.COLImage_2,
        };


        // This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(this,   // Parent activity context
                ImagesProvider.CONTENT_URI,   // Provider content URI to query
                null,             // Columns to include in the resulting Cursor
                SemesterDBHelper.COLImage_3+ "="+id_note,                   // No selection clause
                null,                   // No selection arguments
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        ImgAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        ImgAdapter.swapCursor(null);

    }

    @Override
    public void onBackPressed() {

        Intent i = new Intent(editNote.this,NoteContent.class);
        i.putExtra("sem_id",semid);
        i.putExtra("mat_id",mat_id);
        startActivity(i);
        finish();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
