package com.orcas.orcas_islam.daftary_test1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static android.graphics.Color.BLUE;

public class NoteContent extends AppCompatActivity {
    android.support.design.widget.FloatingActionButton GotONOTE;
    ListView ShowNoteContent;
    customAdapterNote adapter;

    SemesterDBHelper myDB;
    ArrayList<noteData> notes;
    android.support.v7.widget.Toolbar toolbarContent;
     String semId ,MatId;

    DialogInterface.OnClickListener dialogClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_content);

        Intent get =getIntent();
        semId = get.getStringExtra("sem_id");
         MatId = get.getStringExtra("mat_id");
         Toast.makeText(getApplicationContext(),semId,Toast.LENGTH_SHORT).show();

        myDB = new SemesterDBHelper(this);

        ShowNoteContent =(ListView)findViewById(R.id.showNote);

        notes = ShowData(semId , MatId);
        Collections.reverse(notes);
        adapter = new customAdapterNote(getApplicationContext(),R.layout.notecontentdata,notes);

        ShowNoteContent.setAdapter(adapter);

        GotONOTE =(android.support.design.widget.FloatingActionButton) findViewById(R.id.gotonode);
        toolbarContent =(android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbarContent);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarContent.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        toolbarContent.setTitleMarginStart(650);

        GotONOTE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int max = myDB.getID()+1;

               Integer del = myDB.delete(max+"");

               Toast.makeText(getApplicationContext() ,del+"" ,Toast.LENGTH_SHORT).show();

                Intent i = new Intent(view.getContext(),Note.class);
                i.putExtra("sem_id",semId);
                i.putExtra("mat_id",MatId);

                startActivity(i);
            }
        });

        ShowNoteContent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent  F = new Intent(getApplicationContext(),editNote.class);
                F.putExtra("Note_id" ,notes.get(i).getId());
                F.putExtra("Note_title" , notes.get(i).getTitle());
                F.putExtra("Note_Body" , notes.get(i).getBody());
                F.putExtra("sem_id" , notes.get(i).getSem_id());
                F.putExtra("mat_id" , notes.get(i).getMat_id());
                startActivity(F);

            }
        });




        ShowNoteContent.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        ShowNoteContent.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {
                final int checkCount = ShowNoteContent.getCheckedItemCount();
                actionMode.setTitle("تم اختيار "+checkCount +" من العناصر ");


                if (b== false)
                    ShowNoteContent.getChildAt(i).setBackgroundColor(Color.WHITE);
                else
                    ShowNoteContent.getChildAt(i).setBackgroundColor(Color.rgb(204, 221, 255));


                adapter.toggleSelection(i);
            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                getSupportActionBar().hide();
                actionMode.getMenuInflater().inflate(R.menu.menu_action_mode , menu);



                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {

                switch (menuItem.getItemId()){
                    case R.id.delete:

                        final SparseBooleanArray selected = adapter.getSelectedIds();

                                        for (int i=(selected.size() -1); i>=0 ;i--){
                                            if(selected.valueAt(i)){
                                                noteData selectedListItem = adapter.getItem(selected.keyAt(i));
                                                adapter.remove(selectedListItem);
                                                //  Toast.makeText(getApplicationContext(),selectedListItem.getId() +"",Toast.LENGTH_SHORT).show();
                                                myDB.deleteNote(selectedListItem.getId());

                                            }
                                            actionMode.finish();
                                        }

                        return true;
                    default:
                        return false;

                }
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
                adapter.removeSelection();
                getSupportActionBar().show();
                for(int i=0 ; i< adapter.getCount() ;i++){
                    ShowNoteContent.getChildAt(i).setBackgroundColor(Color.WHITE);
                   // ShowNoteContent.getChildAt(i).setBackgroundColor();

                }
            }
        });

    }


    public ArrayList<noteData> ShowData(String sem_id ,String mat_id){
        ArrayList<noteData> notes = new ArrayList<noteData>();

        Cursor res = myDB.getAllDataNote(sem_id , mat_id);
        if (res.getCount() != 0) {
            while (res.moveToNext()) {
                notes.add(new noteData(res.getString(0),res.getString(1),res.getString(2),res.getString(3),res.getString(4)));
                //Toast.makeText(getApplicationContext(),Sem.get(0).getName(),Toast.LENGTH_LONG).show();

            }
        }

        return notes;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(NoteContent.this, Material.class);
        i.putExtra("id",semId);
        startActivity(i);
        finish();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }








}
