package com.orcas.orcas_islam.daftary_test1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import static android.provider.BaseColumns._ID;

public class ShowAdapter extends RecyclerView.Adapter<ShowAdapter.ImagesViewHolder> {

    Context mContext;
    private Cursor mCursor =null;
    SemesterDBHelper myDB;
    ActionMode  actionMode;
    int fragranceName ;
    String id_note , titl , body ,semid ,matid;
    Boolean val =false ;
    DialogInterface.OnClickListener dialogClickListener;


    public ShowAdapter(Context mContext ,String id_note ,String titl ,String body ,String semid ,String matid ,Boolean val) {
        this.mContext = mContext;
        myDB = new SemesterDBHelper(mContext);
        this.id_note =id_note;
        this.titl =titl;
        this.body =body;
        this.semid =semid;
        this.matid =matid;
        this.val=val;
    }

    @Override
    public ImagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.showimage, parent, false);
        return new ImagesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImagesViewHolder holder, final int position) {


       fragranceName = mCursor.getColumnIndex(SemesterDBHelper.COLImage_2);

        mCursor.moveToPosition(position); // get to the right location in the cursor



        final byte[] image = mCursor.getBlob(fragranceName);

        final Bitmap bmp = BitmapFactory.decodeByteArray(image, 0,image.length);


        holder.image.setImageBitmap(bmp);


        dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:

                        myDB.deleteImage(mCursor.getString(0));
                        notifyItemRemoved(position);
                        //  notifyDataSetChanged();
                        //  ShowAdapter.this.notify();
                        ShowAdapter.this.notifyDataSetChanged();
                        // fragranceName = mCursor.getColumnIndex(SemesterDBHelper.COLImage_2);

                        if (val ==false){
                            Intent I = new Intent(mContext,editNote.class);
                            I.putExtra("Note_id",id_note);
                            I.putExtra("Note_title",titl);
                            I.putExtra("Note_Body" ,body);
                            I.putExtra("sem_id",semid);
                            I.putExtra("mat_id",matid);
                            mContext.startActivity(I);}

                        if (val == true){
                            Intent I = new Intent(mContext,Note.class);
                            I.putExtra("Note_title",titl);
                            I.putExtra("Note_Body" ,body);
                            I.putExtra("sem_id",semid);
                            I.putExtra("mat_id",matid);
                            mContext.startActivity(I);}


                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        dialog.dismiss();
                        break;
                }
            }
        };
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

                builder.setMessage("هل تريد حذف الصورة").setPositiveButton("نعم", dialogClickListener)
                        .setNegativeButton("لا", dialogClickListener).show();

            }
        });



    }


    public static class ImagesViewHolder extends RecyclerView.ViewHolder {
        com.github.chrisbanes.photoview.PhotoView image;
        android.support.design.widget.FloatingActionButton deleteButton;
        android.support.v7.widget.RecyclerView rc;
        public ImagesViewHolder(View itemView) {
            super(itemView);

            image = (com.github.chrisbanes.photoview.PhotoView) itemView.findViewById(R.id.showfull);
            deleteButton = (android.support.design.widget.FloatingActionButton)itemView.findViewById(R.id.deleteshow);
            rc =(android.support.v7.widget.RecyclerView)itemView.findViewById(R.id.show);


        }

    }
    @Override
    public int getItemCount() {
        if (mCursor == null) {
            return 0;
        }
        return mCursor.getCount();
    }

    public Cursor swapCursor(Cursor c) {
        // check if this cursor is the same as the previous cursor (mCursor)


        if (mCursor == c) {


            return null; // bc nothing has changed
        }
        Cursor temp = mCursor;
        this.mCursor = c; // new cursor value assigned
        //Toast.makeText(mContext,c+"" ,Toast.LENGTH_SHORT).show();

        //check if this is a valid cursor, then update the cursor
        if (c != null) {
            this.notifyDataSetChanged();
        }
        if(c ==null){
            return null;
        }


        return temp;
    }

}
