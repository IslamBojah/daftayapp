package com.orcas.orcas_islam.daftary_test1;

/**
 * Created by ORCAS_ISLAM on 4/9/2018.
 */

public class MaterialData {

    private String id;
    private String name;
    private  String SemesterId;

    public MaterialData(String id, String name, String semesterId) {
        this.id = id;
        this.name = name;
        SemesterId = semesterId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSemesterId() {
        return SemesterId;
    }

    public void setSemesterId(String semesterId) {
        SemesterId = semesterId;
    }
}
