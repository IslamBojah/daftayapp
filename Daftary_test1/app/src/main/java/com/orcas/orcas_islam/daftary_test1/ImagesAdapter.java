package com.orcas.orcas_islam.daftary_test1;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import static android.provider.BaseColumns._ID;
import java.util.ArrayList;

/**
 * Created by ORCAS_ISLAM on 4/17/2018.
 */

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ImagesViewHolder> {

    // Class variables for the Cursor that holds task data and the Context
    private Cursor mCursor =null;
    private Context mContext;
    private SemesterDBHelper DB;
    private ArrayList<String> arrayImg;
    String note_id , titlenote ,body , semid ,matid;
    boolean value;



    /**
     * Constructor for the CustomCursorAdapter that initializes the Context.
     *
     * @param mContext the current Context
     */
    public ImagesAdapter(Context mContext ) {

        this.mContext = mContext;
        //DB=new SemesterDBHelper(mContext);
    }
    public ImagesAdapter(Context mContext ,ArrayList<String> arr ,String id ,String title,String body ,String semid ,String matid ,boolean value ) {

        this.mContext = mContext;
        this.arrayImg =arr;
        //DB=new SemesterDBHelper(mContext);
        this.note_id=id;
        this.titlenote=title;
        this.body =body;
        this.semid=semid;
        this.matid =matid;
        this.value =value;

    }




    @Override
    public ImagesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.image_item, parent, false);
        return new ImagesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImagesViewHolder holder, final int position) {

        // Indices for the _id, description, and priority columns
        //int idIndex =


       // Log.d(x,"lllllllllllllllllllllllllllllllllllllll");




    int fragranceName = mCursor.getColumnIndex(SemesterDBHelper.COLImage_2);


       mCursor.moveToPosition(position); // get to the right location in the cursor

        // Determine the values of the wanted data
        // final int id = mCursor.getInt(idIndex);
        final byte[] image = mCursor.getBlob(fragranceName);



        //Set values
        // holder.itemView.setTag(id);

        final Bitmap bmp = BitmapFactory.decodeByteArray(image, 0,image.length);


       // holder.image.setImageBitmap(Bitmap.createScaledBitmap(bmp, 200,
        //        200, false));

        holder.image.setImageBitmap(bmp);

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext,arrayImg.get(position)+"",Toast.LENGTH_SHORT).show();

               Intent I =  new Intent(mContext,Full_Image.class);
              // I.putExtra("Image",image);
               I.putExtra("ImageId",arrayImg.get(position));
               I.putExtra("Note_id",note_id);
               I.putExtra("Note_title",titlenote);
               I.putExtra("Note_Body" ,body);
               I.putExtra("sem_id",semid);
               I.putExtra("mat_id",matid);
               I.putExtra("value",value);


               //Toast.makeText(mContext.getApplicationContext(),arrayImg.get(position).toString(),Toast.LENGTH_SHORT).show();
                mContext.startActivity(I);
            }
        });


    }



    @Override
    public int getItemCount() {
        if (mCursor == null) {
            return 0;
        }
        return mCursor.getCount();
    }

    public Cursor swapCursor(Cursor c) {
        // check if this cursor is the same as the previous cursor (mCursor)


        if (mCursor == c) {


            return null; // bc nothing has changed
        }
        Cursor temp = mCursor;
        this.mCursor = c; // new cursor value assigned
        //Toast.makeText(mContext,c+"" ,Toast.LENGTH_SHORT).show();

        //check if this is a valid cursor, then update the cursor
        if (c != null) {
            this.notifyDataSetChanged();
        }
        if(c ==null){
            return null;
        }


        return temp;
    }

    public static class ImagesViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        public ImagesViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.imageProf);
        }

    }
}

