package com.orcas.orcas_islam.daftary_test1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.chrisbanes.photoview.PhotoView;

import static android.provider.BaseColumns._ID;

public class Full_Image extends AppCompatActivity  implements LoaderManager.LoaderCallbacks<Cursor>{
    PhotoView ReImage;
    int IDImage;
    Cursor mCursor;
  public    RecyclerView showImage;
  public    ShowAdapter ShowAdapterview;

    android.support.design.widget.FloatingActionButton del_Image;
    DialogInterface.OnClickListener dialogClickListener;
    SemesterDBHelper  sem;

    android.support.v7.widget.Toolbar toolbarContent;

    String id_note,title,body,semid,mat_id;
    Boolean value;

    private static final int IMAGES_LOADER = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full__image);

        toolbarContent =(android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbarContent);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarContent.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        toolbarContent.setTitleMarginStart(650);


        showImage =(RecyclerView)findViewById(R.id.show);




        Intent re =getIntent();

        //final String x= re.getStringExtra("ImageId");

      //  byte [] image =re.getByteArrayExtra("Image");

       id_note = re.getStringExtra("Note_id");
       title = re.getStringExtra("Note_title");
       body = re.getStringExtra("Note_Body");
       semid = re.getStringExtra("sem_id");
       mat_id = re.getStringExtra("mat_id");
        value = re.getBooleanExtra("value" ,false);

      //  IDImage = Integer.parseInt(x);

     //   Toast.makeText(getApplicationContext(),id_note+"we",Toast.LENGTH_SHORT).show();



        ShowAdapterview = new ShowAdapter(this , id_note , title ,body ,semid,mat_id ,value);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        showImage.setLayoutManager(horizontalLayoutManagaer);
        showImage.setAdapter(ShowAdapterview);

        getLoaderManager().initLoader(IMAGES_LOADER, null, this);


    }




    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                SemesterDBHelper.COLImage_2,
        };

        // This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(this,   // Parent activity context
                ImagesProvider.CONTENT_URI,   // Provider content URI to query
                null,             // Columns to include in the resulting Cursor
                SemesterDBHelper.COLImage_3+ "="+id_note,                   // No selection clause
                null,                   // No selection arguments
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        ShowAdapterview.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        ShowAdapterview.swapCursor(null);

    }

    public Cursor swapCursor(Cursor c) {
        // check if this cursor is the same as the previous cursor (mCursor)


        if (mCursor == c) {


            return null; // bc nothing has changed
        }
        Cursor temp = mCursor;
        this.mCursor = c; // new cursor value assigned
        //Toast.makeText(mContext,c+"" ,Toast.LENGTH_SHORT).show();

        //check if this is a valid cursor, then update the cursor
        if (c != null) {
            //this.notifyDataSetChanged();
        }
        if(c ==null){
            return null;
        }


        return temp;
    }

    public void onBackPressed() {

        if (value == false){
            Intent I = new Intent(getApplicationContext(),editNote.class);
            I.putExtra("Note_id",id_note);
            I.putExtra("Note_title",title);
            I.putExtra("Note_Body" ,body);
            I.putExtra("sem_id",semid);
            I.putExtra("mat_id",mat_id);
            startActivity(I);}

        if (value == true){
            Intent I = new Intent(getApplicationContext(),Note.class);
            I.putExtra("Note_title",title);
            I.putExtra("Note_Body" ,body);
            I.putExtra("sem_id",semid);
            I.putExtra("mat_id",mat_id);
            startActivity(I);}

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }






}
