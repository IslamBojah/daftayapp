package com.orcas.orcas_islam.daftary_test1;

import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Handler;
import android.provider.BaseColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

public class Note extends AppCompatActivity  implements LoaderManager.LoaderCallbacks<Cursor>{


    Button addnote ;
    EditText TitleNote,BodyNote;
    SemesterDBHelper SDH;
    Button AddImage;

    ImagesAdapter imagesAdapter;
    RecyclerView mRecyclerView;
ArrayList<String> arrayOFImage;
ArrayList<String> pro =new ArrayList<String>();
String[] ImCo;
    android.support.v7.widget.Toolbar toolbarContent;

     String semid,matid,title,body;

    private static final int IMAGES_LOADER = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        arrayOFImage =new ArrayList<String>();

        toolbarContent =(android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbarContent);

        toolbarContent.setTitleMarginStart(650);
        toolbarContent.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        SDH = new SemesterDBHelper(this);
/*
        Cursor c =SDH.getImage(SDH.getID()+1+"");

        if(c.getCount() !=0){

            while(c.moveToNext()){


               String y = c.getString(0);
               Toast.makeText(getApplicationContext(),y+"we",Toast.LENGTH_SHORT).show();
            }
        }


*/

        addnote =(Button) findViewById(R.id.addnote);
        TitleNote = (EditText) findViewById(R.id.noteTitle);
        BodyNote = (EditText) findViewById(R.id.BodyNode);
        AddImage = (Button) findViewById(R.id.Add_image);

        Intent i = getIntent();
        semid = i.getStringExtra("sem_id");
       matid = i.getStringExtra("mat_id");
        title =i.getStringExtra("Note_title");
         body =i.getStringExtra("Note_Body");


        TitleNote.setText(title);
        BodyNote.setText(body);




        Cursor c =SDH.getImage(SDH.getID()+1+"");

        if(c.getCount() !=0){

            while(c.moveToNext()){


                String y = c.getString(0);
                arrayOFImage.add(y);
                // Toast.makeText(getApplicationContext(),y+"we",Toast.LENGTH_SHORT).show();
            }
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.images);

        TitleNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
               // Toast.makeText(getApplicationContext(),"welcome",Toast.LENGTH_SHORT).show();
                imagesAdapter = new ImagesAdapter(getApplicationContext() ,arrayOFImage ,SDH.getID()+1+"",TitleNote.getText().toString(),BodyNote.getText().toString(),semid,matid ,true);
                mRecyclerView.setAdapter(imagesAdapter);
                getLoaderManager().initLoader(IMAGES_LOADER, null, Note.this);

            }
        });


        BodyNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Toast.makeText(getApplicationContext(),"welcome",Toast.LENGTH_SHORT).show();
                imagesAdapter = new ImagesAdapter(getApplicationContext() ,arrayOFImage ,SDH.getID()+1+"",TitleNote.getText().toString(),BodyNote.getText().toString(),semid,matid ,true);
                mRecyclerView.setAdapter(imagesAdapter);
                getLoaderManager().initLoader(IMAGES_LOADER, null, Note.this);

            }
        });


       // mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(horizontalLayoutManagaer);

        imagesAdapter = new ImagesAdapter(this ,arrayOFImage ,SDH.getID()+1+"",TitleNote.getText().toString(),BodyNote.getText().toString(),semid,matid ,true);
        mRecyclerView.setAdapter(imagesAdapter);

        getLoaderManager().initLoader(IMAGES_LOADER, null, this);





       AddImage.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent i = new Intent(Note.this ,AddnewImage.class);
               i.putExtra("sem_id",semid);
               i.putExtra("mat_id",semid);
               i.putExtra("Note_title",TitleNote.getText().toString());
               i.putExtra("Note_Body",BodyNote.getText().toString());
               i.putExtra("value",true);
               startActivity(i);
           }
       });


       addnote.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               boolean b;
              b= SDH.insertDataNote(TitleNote.getText().toString(),BodyNote.getText().toString(),semid,matid);
              if(b==true){
                  Toast.makeText(getApplicationContext(),"dataInsert",Toast.LENGTH_SHORT).show();
                  Intent i = new Intent(getApplicationContext(), NoteContent.class);
                  i.putExtra("sem_id",semid);
                  i.putExtra("mat_id",matid);

                  startActivity(i);

              }
              else{
                  Toast.makeText(getApplicationContext(),"Wrong",Toast.LENGTH_SHORT).show();
              }
           }
       });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                SemesterDBHelper.COLImage_2,
        };

        int max=SDH.getID()+1;
        // This loader will execute the ContentProvider's query method on a background thread
        return new CursorLoader(this,   // Parent activity context
                ImagesProvider.CONTENT_URI,   // Provider content URI to query
                projection,             // Columns to include in the resulting Cursor
                SemesterDBHelper.COLImage_3+ "="+max,                   // No selection clause
                null,                   // No selection arguments
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        imagesAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        imagesAdapter.swapCursor(null);

    }

    @Override
    public void onBackPressed() {
      //  super.onBackPressed();
        Intent i = new Intent(Note.this,NoteContent.class);
        i.putExtra("sem_id",semid);
        i.putExtra("mat_id",matid);
        startActivity(i);
        finish();

    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
