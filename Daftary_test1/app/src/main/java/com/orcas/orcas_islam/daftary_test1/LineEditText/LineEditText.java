package com.orcas.orcas_islam.daftary_test1.LineEditText;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;

/**
 * Created by ORCAS_ISLAM on 4/16/2018.
 */


public class LineEditText extends android.support.v7.widget.AppCompatEditText {
    private Rect mRect;
    private Paint mPaint;

    int initialCount = 0;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public LineEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        mRect = new Rect();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(Color.BLUE);
        initialCount = getMinLines();
        setLines(initialCount);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        // Gets the number of lines of text in the View.
        int count = getBaseline();
        // Gets the global Rectangle and Paint objects
        Rect r = mRect;
        Paint paint = mPaint;
        // Gets the baseline coordinates for the current line of text
        int baseline = getLineBounds(0, r);
        // Draws one line in the rectangle for every line of text in the EditText
        for (int i = 0; i < count; i++) {
            /*
             * Draws a line in the background from the left of the rectangle to the
             * right, at a vertical position one dip below the baseline, using the
             * "paint" object for details.
             */
            canvas.drawLine(r.left, baseline + 1, r.right, baseline + 1, paint);
            baseline += getLineHeight();//next line
        }

        // Finishes up by calling the parent method
        super.onDraw(canvas);
    }


}