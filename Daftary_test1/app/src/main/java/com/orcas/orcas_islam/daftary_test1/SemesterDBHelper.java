package com.orcas.orcas_islam.daftary_test1;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static android.provider.BaseColumns._ID;


/**
 * Created by ORCAS_ISLAM on 4/5/2018.
 */


// class to help dealing with database extends SQLiteOpenHelper with onCreate and onUpgarade ......
public class SemesterDBHelper extends SQLiteOpenHelper {

     // declare database items .....
    public static final String  DATABASE_NAME = "Semester.db";
    public static final String  TABLE_NAME = "semester_table";
    public static final String  COL_1 = "Sem_id";
    public static final String  COL_2 = "Sem_name";
    public static final String  COL_3 = "sem_date";


    // Material Table
    public static final String  TABLE_NAME2 = "material_table";
    public static final String  COLMaterial_1 = "mat_id";
    public static final String  COLMaterial_2 = "mat_name";
    public static final String  COLMaterial_3 = "mats_id";


    //Note Table
    public static final String TABLE_NAME3 = "note_table";
    public static final String COLNote_1 = "note_id";
    public static final String COLNote_2 = "note_title";
    public static final String COLNote_3 = "note_body";
    public static final String COLNote_4 = "note_sem_id";
    public static final String COLNote_5 = "note_mat_id";





    // Image Table
    public static final String  TABLE_NAME4 = "image_table";
   // public static final String COLImage_1 = "image_id";
    public static final String COLImage_2 = "image_name";
    public static final String COLImage_3 = "image_note_id";



    ContentResolver mContentResolver;


    // create database with item type ......

    public static final String DATABASE_CREATE = "create table "+ TABLE_NAME + " (" + COL_1 + " integer primary key autoincrement," + COL_2 + " text not null,"+ COL_3 + " date not null);" ;

    public static final String Material_CREATE = "create table "+ TABLE_NAME2 + " (" + COLMaterial_1 + " integer primary key autoincrement," + COLMaterial_2 + " text not null,"+COLMaterial_3 +" text not null);" ;

    public static final String note_CREATE = "create table "+ TABLE_NAME3 + " (" + COLNote_1 + " integer primary key autoincrement," + COLNote_2 + " text not null,"+ COLNote_3+ " text not null,"+ COLNote_4 + " text not null,"+COLNote_5 +" text not null);" ;

    public final String SQL_CREATE_IMAGE_TABLE = "CREATE TABLE " + TABLE_NAME4 + " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLImage_2 + " BLOB NOT NULL , " + COLImage_3  + " TEXT NOT NULL " +  " );";

    // constructor function for helper (context , dataBase name , null , version number ) ....
    public SemesterDBHelper(Context context) {

        super(context, DATABASE_NAME, null, 9);

        mContentResolver = context.getContentResolver();


    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

       // on create to create dataBase
        sqLiteDatabase.execSQL(Material_CREATE);
        sqLiteDatabase.execSQL(DATABASE_CREATE);
        sqLiteDatabase.execSQL(note_CREATE);
        sqLiteDatabase.execSQL(SQL_CREATE_IMAGE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {


        // to delete data base
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_NAME );
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_NAME2 );
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_NAME3 );
        sqLiteDatabase.execSQL("drop table if exists " + TABLE_NAME4 );

        this.onCreate(sqLiteDatabase);
    }


     // this function to insert data to dataBase ......

    public  boolean insertData(String Semester_name, String Semester_date){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2,Semester_name);
        contentValues.put(COL_3,Semester_date);


        long result = db.insert(TABLE_NAME , null, contentValues);

        if (result == -1)
            return false;
        else
            return true;

    }




    // this function to get all data at database .....
    public Cursor getAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+ TABLE_NAME , null);
        return res;

    }


    // insert data to Marterial Table //////////////////////////

    public  boolean insertDataMaterial(String materialName, String SMID){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLMaterial_2,materialName);
        contentValues.put(COLMaterial_3,SMID);

        long result = db.insert(TABLE_NAME2 , null, contentValues);

        if (result == -1)
            return false;
        else
            return true;

    }


    public Cursor getAllDataMaterial(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+ TABLE_NAME2 +" WHERE " + COLMaterial_3 + " = " +id , null);
        return res;

    }

    public  boolean insertDataNote(String noteTitle, String noteBody , String note_Sem_id , String note_mat_id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLNote_2,noteTitle);
        contentValues.put(COLNote_3,noteBody);
        contentValues.put(COLNote_4,note_Sem_id);
        contentValues.put(COLNote_5,note_mat_id);

        long result = db.insert(TABLE_NAME3 , null, contentValues);

        if (result == -1)
            return false;
        else
            return true;

    }

    public Cursor getAllDataNote(String sem_id , String mat_id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+ TABLE_NAME3 +" WHERE " + COLNote_4 + " = " +sem_id + " AND " +COLNote_5 +" = " +mat_id , null);
        return res;

    }

    public boolean updateNote(String id , String Title , String body , String sem_id , String mat_id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLNote_1, id);
        contentValues.put(COLNote_2, Title);
        contentValues.put(COLNote_3, body);
        contentValues.put(COLNote_4, sem_id);
        contentValues.put(COLNote_5, mat_id);

        db.update(TABLE_NAME3 , contentValues ,"note_id = ?",new String[]{id});
        return true;
    }


    /// Add data to image TABLE :----------

  public  void insertDataImage(byte[] image , String noteID){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLImage_2,image);
        contentValues.put(COLImage_3,noteID);


        db.insert(TABLE_NAME4 , null, contentValues);

    }

    public Cursor getImage( String mat_id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+ TABLE_NAME4 +" WHERE " + COLImage_3 + " = " +mat_id , null);
        return res;

    }

    public Cursor getLastNote(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+ TABLE_NAME3 , null);
        return res;

    }

    public int getID()
    {
        SQLiteDatabase database=this.getReadableDatabase();
        Cursor cursor = database.rawQuery("SELECT MAX("+COLNote_1+") FROM "+TABLE_NAME3, null);
        int maxid = (
                cursor.moveToFirst() ? cursor.getInt(0) : 0);
        return maxid;
    }

    public Integer delete(String max){
        SQLiteDatabase db =this.getWritableDatabase();
        return db.delete(TABLE_NAME4 ,COLImage_3 +" = ?" ,new String[] {max});
    }

    public Integer deleteImage(String max){
        SQLiteDatabase db =this.getWritableDatabase();
        return db.delete(TABLE_NAME4 ,_ID +" = ?" ,new String[] {max});
    }

    public void deleteNote(String noteid){
        SQLiteDatabase db =this.getWritableDatabase();
         db.delete(TABLE_NAME3 ,COLNote_1 +" = ?" ,new String[] {noteid});
    }

}
