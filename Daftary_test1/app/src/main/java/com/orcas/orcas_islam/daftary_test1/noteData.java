package com.orcas.orcas_islam.daftary_test1;

/**
 * Created by ORCAS_ISLAM on 4/15/2018.
 */

public class noteData  {
    String id;
    String title;
    String body;
    String sem_id ;
    String mat_id;

    public noteData(String id, String title, String body, String sem_id, String mat_id) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.sem_id = sem_id;
        this.mat_id = mat_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSem_id() {
        return sem_id;
    }

    public void setSem_id(String sem_id) {
        this.sem_id = sem_id;
    }

    public String getMat_id() {
        return mat_id;
    }

    public void setMat_id(String mat_id) {
        this.mat_id = mat_id;
    }
}
