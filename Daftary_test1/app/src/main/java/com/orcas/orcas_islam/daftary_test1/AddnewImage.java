package com.orcas.orcas_islam.daftary_test1;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.Toast;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AddnewImage extends AppCompatActivity  implements View.OnClickListener{


    private ImageView profileImageView;
    private Button pickImage;

    private static final int SELECT_PHOTO = 1;
    private static final int CAPTURE_PHOTO = 2;

    private ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarbHandler = new Handler();
    private boolean hasImageChanged = false;
    SemesterDBHelper dbHelper;
    ArrayList<String> lastid;

    String semid,matid;

    Bitmap thumbnail;

    int x;
    android.support.v7.widget.Toolbar toolbarContent;

    String notetitle , notebody , note_id;
    boolean value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnew_image);

        toolbarContent =(android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbarContent);
        toolbarContent.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbarContent.setTitleMarginStart(650);


        Intent i =getIntent();
        semid = i.getStringExtra("sem_id");
        matid = i.getStringExtra("mat_id");
        note_id=i.getStringExtra("note_id");
        notetitle=i.getStringExtra("Note_title");
        notebody =i.getStringExtra("Note_Body");
        value=i.getBooleanExtra("value",true);


        profileImageView = (ImageView) findViewById(R.id.profileImageView);
        pickImage = (Button) findViewById(R.id.pick_image);

        pickImage.setOnClickListener(this);

        if (ContextCompat.checkSelfPermission(AddnewImage.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            profileImageView.setEnabled(false);
            ActivityCompat.requestPermissions(AddnewImage.this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        } else {
            profileImageView.setEnabled(true);
        }





     //  int y = lastid.size()+1;
       //Toast.makeText(this,y+"",Toast.LENGTH_SHORT).show();


        dbHelper  = new SemesterDBHelper(this);
       x= dbHelper.getID();
        Toast.makeText(this,x+"",Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
        case R.id.pick_image:
        new MaterialDialog.Builder(this)
                .itemsGravity(GravityEnum.END)
                .titleGravity(GravityEnum.END)
                .title(R.string.uploadImages)
                .items(R.array.uploadImages)
                .itemsIds(R.array.itemIds)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which){
                            case 0:
                                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                                photoPickerIntent.setType("image/*");
                                startActivityForResult(photoPickerIntent,SELECT_PHOTO);
                                break;
                            case 1:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, CAPTURE_PHOTO);
                                break;
                            case 2:
                                profileImageView.setImageDrawable(null);
                                break;
                        }
                    }
                })
                .show();

              //  MaterialDialog.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);


            break;

    }
    }

    public void setProgressBar(){
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(true);
        progressBar.setMessage("أنتظر ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(50);
        progressBar.show();
        progressBarStatus = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progressBarStatus < 50){
                    progressBarStatus += 30;

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    progressBarbHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setProgress(progressBarStatus);
                        }
                    });
                }
                if (progressBarStatus >= 50) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    progressBar.dismiss();
                }

            }
        }).start();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SELECT_PHOTO){
            if(resultCode == RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    final InputStream imageStream = getContentResolver().openInputStream(imageUri);

                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);


                    //set Progress Bar
                    setProgressBar();
                    //set profile picture form gallery
                    //profileImageView.setImageBitmap(selectedImage);

                    profileImageView.setImageBitmap(selectedImage);



                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

        }else if(requestCode == CAPTURE_PHOTO){
            if(resultCode == RESULT_OK) {
                onCaptureImageResult(data);
            }
        }
    }




    private void onCaptureImageResult(Intent data) {
      //  thumbnail = MediaStore.Images.Media.getBitmap(getContentResolver(),data);
        thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

       // thumbnail.compress(Bitmap.CompressFormat.JPEG, 40, bytes);

        //set Progress Bar
        setProgressBar();

      //  profileImageView.setImageBitmap(Bitmap.createScaledBitmap(thumbnail, 1920,
          //      1080, true));
        profileImageView.setImageBitmap(thumbnail);

    }

    public void addToDb(View view){

        profileImageView.setDrawingCacheEnabled(true);
        profileImageView.buildDrawingCache();
        Intent i = new Intent(AddnewImage.this, Note.class);
        i.putExtra("sem_id", semid);
        i.putExtra("mat_id", matid);
        i.putExtra("Note_title", notetitle);
        i.putExtra("Note_Body", notebody);


        Intent j = new Intent(AddnewImage.this, editNote.class);
        j.putExtra("sem_id", semid);
        j.putExtra("mat_id", matid);
        j.putExtra("Note_title", notetitle);
        j.putExtra("Note_Body", notebody);
        j.putExtra("Note_id", note_id);


        if (profileImageView.getDrawable() != null) {
            Bitmap bitmap = profileImageView.getDrawingCache();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();


            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();


            if (value == true) {
                int max = dbHelper.getID() + 1;
                dbHelper.insertDataImage(data, max + "");
                Toast.makeText(this, "Image saved to DB successfully", Toast.LENGTH_SHORT).show();

                startActivity(i);
            } else {
                dbHelper.insertDataImage(data, note_id + "");
                Toast.makeText(this, "Image saved to DB successfully", Toast.LENGTH_SHORT).show();

                startActivity(j);
            }
        }
        else
        {
           // Toast.makeText(getApplicationContext(),"welcomeee", Toast.LENGTH_SHORT).show();
            if (value == true){
                startActivity(i);
            }
            else
                startActivity(j);
        }

    }

    @Override
    public void onBackPressed() {

        if(value==true){
            Intent i =new Intent(AddnewImage.this ,Note.class);
            i.putExtra("sem_id",semid);
            i.putExtra("mat_id",matid);
            i.putExtra("Note_title",notetitle);
            i.putExtra("Note_Body",notebody);
            startActivity(i);}
        else {
            Intent i =new Intent(AddnewImage.this ,editNote.class);
            i.putExtra("sem_id",semid);
            i.putExtra("mat_id",matid);
            i.putExtra("Note_title",notetitle);
            i.putExtra("Note_Body",notebody);
            i.putExtra("Note_id",note_id);
            startActivity(i);
        }


    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
