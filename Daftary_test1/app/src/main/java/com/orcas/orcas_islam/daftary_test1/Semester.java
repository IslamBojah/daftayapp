package com.orcas.orcas_islam.daftary_test1;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.icu.util.ULocale;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


import static java.lang.Integer.getInteger;
import static java.lang.Integer.parseInt;

public class Semester extends AppCompatActivity{

    android.support.v7.widget.Toolbar toolbar;
    SemesterDBHelper myDB ;
    EditText semName, semDate;

    Button addData , ViewAll  ,addDataSemester , cancelAlert ;
    android.support.design.widget.FloatingActionButton AddSemester;
    ListView ls;
    ArrayList<semesterData> Sem;
    customAdapterSem adapter;

    android.support.v7.widget.Toolbar toolbarContent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_semester);



        toolbarContent =(android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbarContent);

       // toolbarContent.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        toolbarContent.setTitleMarginStart(650);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myDB = new SemesterDBHelper(this);


       // addData =(Button)findViewById(R.id.addData);
       // ViewAll = (Button)findViewById(R.id.AllData);
        AddSemester =(android.support.design.widget.FloatingActionButton)findViewById(R.id.Addnew);
        ls =(ListView)findViewById(R.id.semester);


        Sem = ShowData();
        Collections.reverse(Sem);
        adapter = new customAdapterSem(getApplication(), R.layout.custom_semester, Sem);
        ls.setAdapter(adapter);



   ls.setOnItemClickListener(new AdapterView.OnItemClickListener() {
       @Override
       public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

           ArrayList<semesterData> Sem = ShowData();
           Collections.reverse(Sem);
           adapter = new customAdapterSem(getApplication(), R.layout.custom_semester, Sem );
           ls.setAdapter(adapter);

           String b =Sem.get(i).getId();
           Intent f = new Intent(getApplicationContext() , Material.class);
           f.putExtra("id", b);
         startActivity(f);
       }
   });




       alertMessage ();


    }


    public void alertMessage (){



        AddSemester.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(Semester.this);
                final View Mview = getLayoutInflater().inflate(R.layout.semester_dialog,null);

                semName = (EditText)Mview.findViewById(R.id.Semestername_ET);
                semDate = (EditText)Mview.findViewById(R.id.SemesterDate_ET);

                addDataSemester = (Button) Mview.findViewById(R.id.addDataSemester) ;
                cancelAlert=(Button)Mview.findViewById(R.id.CancelAlert);




                mBuilder.setCancelable(true);
                mBuilder.setTitle("أضف فصل جديد ");
                mBuilder.setView(Mview);

                final AlertDialog dialog = mBuilder.create();

                dialog.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                dialog.show();


                semDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //Toast.makeText(getContext(),"welcomeee",Toast.LENGTH_SHORT).show();

                        Calendar calendar=Calendar.getInstance();
                       // Calendar.getInstance().
                      //  DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
                        int d =calendar.get(Calendar.DAY_OF_MONTH);
                        int m = calendar.get(Calendar.MONTH);
                        int y = calendar.get(Calendar.YEAR);
                        DatePickerDialog pickerDialog = new DatePickerDialog(Semester.this, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                                int m=i1+1;
                                semDate.setText(i+"/"+m+"/"+i2);
                            }
                        },y,m,d);
                      ///  pickerDialog.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                        pickerDialog.show();

                    }
                });




                addDataSemester.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        boolean isInserted = false;
                        if (semDate.getText().toString()!= "" || semName.getText().toString() != ""){
                        isInserted =  myDB.insertData(semName.getText().toString(),semDate.getText().toString());}

                        if(isInserted == true)
                            Toast.makeText(getApplicationContext(),"Data Inserted" ,Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(getApplicationContext(),"Data not Inserted" ,Toast.LENGTH_LONG).show();


                        dialog.dismiss();

                     //   adapter.add(new semesterData(semName.getText().toString(),semDate.getText().toString())));
                        ArrayList<semesterData> Sem = ShowData();
                        Collections.reverse(Sem);
                        adapter = new customAdapterSem(getApplication(), R.layout.custom_semester, Sem );
                        ls.setAdapter(adapter);

                        //Intent i = new Intent(getApplicationContext() ,Semester.class);
                        //startActivity(i);
                    }
                });

                cancelAlert.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       dialog.dismiss();

                    }
                });
            }
        });
    }

    public ArrayList<semesterData> ShowData(){
        ArrayList<semesterData> Sem = new ArrayList<semesterData>();

            Cursor res = myDB.getAllData();
            if (res.getCount() != 0) {

                while (res.moveToNext()) {
                    Sem.add(new semesterData(res.getString(0),res.getString(1),res.getString(2)));
                    //Toast.makeText(getApplicationContext(),Sem.get(0).getName(),Toast.LENGTH_LONG).show();

                }
            }

        return Sem;
        }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }






}

