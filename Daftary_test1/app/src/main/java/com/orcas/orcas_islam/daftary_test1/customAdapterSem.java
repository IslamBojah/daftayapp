package com.orcas.orcas_islam.daftary_test1;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Random;
import java.util.zip.Inflater;

/**
 * Created by ORCAS_ISLAM on 4/8/2018.
 */

public class customAdapterSem extends ArrayAdapter<semesterData> {

    private SparseBooleanArray selectedListItemIds;
    ArrayList<semesterData> products;
    Context context ;
    int resource;
    int m=0;



    public customAdapterSem(@NonNull Context context, int resource, ArrayList<semesterData> products) {
        super(context, resource ,products);

        this.products = products;
        this.context = context;
        this.resource = resource;
        selectedListItemIds = new SparseBooleanArray();

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null){
            LayoutInflater layoutInflater=(LayoutInflater) getContext()
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView=layoutInflater.inflate(R.layout.custom_semester,null,true);
        }

        final semesterData product=getItem(position);

        final TextView textName=(TextView) convertView.findViewById(R.id.semstre_name);
        textName.setText(product.getName());

        final TextView textdate=(TextView) convertView.findViewById(R.id.semstre_date);
        textdate.setText(product.getDate());

        final android.support.v7.widget.CardView  cw = (android.support.v7.widget.CardView)convertView.findViewById(R.id.semesterCard);

        String[] col = {"#FF8A80","#FF80AB","#EA80FC","#82B1FF","#84FFFF","#CCFF90","#FFFF8D","#FFAB40"  ,

        };
        cw.setCardBackgroundColor(Color.parseColor(col[m]));
        m=m+1;
        if(m==8){
            m=0;
        }

        return convertView;
    }




    public void toggleSelection(int position ){

        selectView(position , !selectedListItemIds.get(position));
    }

    public void selectView(int  position , boolean value ){
        if (value)
            selectedListItemIds.put(position,value);

        else
            selectedListItemIds.delete(position);
        notifyDataSetChanged();
    }

    public void removeSelection (){
        selectedListItemIds =new SparseBooleanArray();
        notifyDataSetChanged();
    }
    public int getSelectedCount(){
        return selectedListItemIds.size();
    }
    public SparseBooleanArray getSelectedIds (){
        return selectedListItemIds;
    }


}





