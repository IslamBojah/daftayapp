package com.orcas.orcas_islam.daftary_test1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by ORCAS_ISLAM on 4/15/2018.
 */

public class customAdapterNote extends ArrayAdapter<noteData> {
    private SparseBooleanArray selectedListItemIds;
    ArrayList<noteData> products;
    Context context ;
    int resource;

    int m=0;

    public customAdapterNote(@NonNull Context context, int resource , @NonNull ArrayList<noteData> products) {
        super(context, resource ,products);
        this.context = context;
        this.resource = resource;
        this.products = products;
        selectedListItemIds = new SparseBooleanArray();

    }




    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){

            LayoutInflater layoutInflater=(LayoutInflater) getContext()
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            convertView=layoutInflater.inflate(R.layout.notecontentdata,null,true);

        }
        final noteData product=getItem(position);



        final  TextView title= (TextView)convertView.findViewById(R.id.ls_noteTitle);
        title.setText(product.getTitle());

        final TextView body = (TextView)convertView.findViewById(R.id.ls_noteBody);
        body.setText(product.getBody());

        final android.support.v7.widget.CardView cw =(android.support.v7.widget.CardView)convertView.findViewById(R.id.noteCard);

        String[] col = {"#FF8A80","#FF80AB","#EA80FC","#82B1FF","#84FFFF","#CCFF90","#FFFF8D","#FFAB40"  ,

        };
        cw.setCardBackgroundColor(Color.parseColor(col[m]));
       /* final TextView tv =(TextView)convertView.findViewById(R.id.textNote);
        tv.setText(num+"");
        num = num+1;*/

        m=m+1;
        if(m==8){
            m=0;
        }

        return convertView;
    }


    public void toggleSelection(int position ){

        selectView(position , !selectedListItemIds.get(position));
    }

    public void selectView(int  position , boolean value ){
        if (value)
            selectedListItemIds.put(position,value);

        else
            selectedListItemIds.delete(position);
        notifyDataSetChanged();
    }

    public void removeSelection (){
        selectedListItemIds =new SparseBooleanArray();
        notifyDataSetChanged();
    }
    public int getSelectedCount(){
        return selectedListItemIds.size();
    }
    public SparseBooleanArray getSelectedIds (){
        return selectedListItemIds;
    }
}
