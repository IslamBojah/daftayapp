package com.orcas.orcas_islam.daftary_test1;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.Random;

import static android.graphics.Color.WHITE;

/**
 * Created by ORCAS_ISLAM on 1/24/2018.
 */

class customAdpaterMaterial extends RecyclerView.Adapter<customAdpaterMaterial.ViewHolder> {

    private ArrayList<MaterialData> Materials;
    private Context context;
   // Random r1 = new Random();
    SemesterDBHelper dp;
    int m=0;


    public customAdpaterMaterial(Context context, ArrayList<MaterialData> Materials) {
        this.Materials = Materials;
        this.context = context;
        dp =new SemesterDBHelper(context);

    }

    @Override
    public customAdpaterMaterial.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cutom_material_data, viewGroup, false);
        return new ViewHolder(view);


    }

    @Override
    public void onBindViewHolder(final customAdpaterMaterial.ViewHolder viewHolder, final int i) {


        viewHolder.title.setText(Materials.get(i).getName());
      //  viewHolder.cw.setCardBackgroundColor();

        final String x= Materials.get(i).getId();
        final String y = Materials.get(i).getSemesterId();




        viewHolder.cw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(),NoteContent.class);
                i.putExtra("sem_id",y);
                i.putExtra("mat_id",x);
                //Toast.makeText(view.getContext(),x+y+"welcome",Toast.LENGTH_SHORT).show();
                view.getContext().startActivity(i);

            }
        });

     String[] col = {"#FF8A80","#FF80AB","#EA80FC","#82B1FF","#84FFFF","#CCFF90","#FFFF8D","#FFAB40"  ,

       };


    //  viewHolder.cw.setCardBackgroundColor(Color.parseColor(col[i]));
    // viewHolder.cw.setBackgroundColor(Color.parseColor(col[i]));

//        viewHolder.mm1.setBackgroundColor(Color.parseColor("#82B1FF"));
       viewHolder.mm1.setBackgroundColor(Color.parseColor(col[m]));
       // viewHolder.image1.setBackgroundColor(Color.parseColor(col[i]));

        m=m+1;
        if(m==8){
            m=0;
        }
    }

    @Override
    public int getItemCount() {
        return Materials.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private ImageView image1,image2;
        android.support.v7.widget.CardView cw;
        LinearLayout mm1;

        public ViewHolder(View view) {
            super(view);


            cw =(android.support.v7.widget.CardView)view.findViewById(R.id.cw1);
            title = (TextView) view.findViewById(R.id.title_Material);
            image1 =(ImageView) view.findViewById(R.id.img_album);
            mm1=(LinearLayout)view.findViewById(R.id.mm1);
            view.setBackgroundColor(WHITE);



        }
    }

    public void AddMaterials(String x ,String y){
        dp.insertDataMaterial(x,y);
        notifyDataSetChanged();

    }
    public ArrayList<MaterialData> ShowData(String id){
        ArrayList<MaterialData> Sem = new ArrayList<MaterialData>();

        Cursor res = dp.getAllDataMaterial(id);
        if (res.getCount() != 0) {

            while (res.moveToNext()) {
                Sem.add(new MaterialData(res.getString(0),res.getString(1),res.getString(2)));
                //Toast.makeText(getApplicationContext(),Sem.get(0).getName(),Toast.LENGTH_LONG).show();
                notifyDataSetChanged();

            }
        }

        return Sem;
    }

}
