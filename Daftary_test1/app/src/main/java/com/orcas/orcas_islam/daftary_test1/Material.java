package com.orcas.orcas_islam.daftary_test1;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.ArrayList;
import java.util.Collections;

public class Material extends AppCompatActivity {

    RecyclerView MaterialsV;
    SemesterDBHelper Mdp;
    ArrayList<MaterialData> Marraylist ;
    Button  add, cancel;
    android.support.design.widget.FloatingActionButton addMaterial;
    EditText materialName;

    RecyclerView recyclerView;
    ArrayList<MaterialData> Sem;
    customAdpaterMaterial adapter;
    android.support.v7.widget.Toolbar toolbarMaterial;
    String id,IdPause;
    Intent i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material);


         i=getIntent();
         id = i.getStringExtra("id");



        toolbarMaterial =(android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbarMaterial);
        toolbarMaterial.setTitleMarginStart(650);
        toolbarMaterial.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);




      //  getSupportActionBar().setTitle("Material");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

       // Toast.makeText(getApplicationContext(),id ,Toast.LENGTH_SHORT).show();

        recyclerView = (RecyclerView) findViewById(R.id.material_data);

        Mdp =new SemesterDBHelper(this);


        addMaterial =(FloatingActionButton)findViewById(R.id.addMaterial);

        Sem = ShowData(id);
        //Collections.reverse(Sem);

        //recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
         adapter = new customAdpaterMaterial(getApplicationContext(), Sem);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);

        addData(id);


    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("IdMaterial",id);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState == null) return;

     //   id=savedInstanceState.getString("IdMaterial");

    }

    public void addData(final String id){
        addMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(Material.this);
                final View Mview = getLayoutInflater().inflate(R.layout.popup_martials,null);

                add =(Button)Mview.findViewById(R.id.material_add) ;
                cancel=(Button)Mview.findViewById(R.id.material_cancel);

             materialName =(EditText)Mview.findViewById(R.id.material_Name);

                mBuilder.setCancelable(true);
                mBuilder.setTitle("أضف مادة جديدة");
                mBuilder.setView(Mview);
                final AlertDialog dialog = mBuilder.create();
                dialog.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                dialog.show();

                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        boolean isInserted = false;
                        if (materialName.getText().toString()!=""){

                            isInserted =  Mdp.insertDataMaterial(materialName.getText().toString(),id);
                        }

                        if(isInserted == true){
                            Toast.makeText(getApplicationContext(),"Data Inserted" ,Toast.LENGTH_SHORT).show();
                            Sem = ShowData(id);

                           // recyclerView.setHasFixedSize(true);
                            adapter = new customAdpaterMaterial(getApplicationContext(), Sem);
                            adapter.notifyDataSetChanged();
                            recyclerView.setAdapter(adapter);

                        }


                        else
                            Toast.makeText(getApplicationContext(),"Data not Inserted" ,Toast.LENGTH_LONG).show();

                        dialog.dismiss();



                    }
                });


                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });

    }

    public ArrayList<MaterialData> ShowData(String id){
        ArrayList<MaterialData> Sem = new ArrayList<MaterialData>();

        Cursor res = Mdp.getAllDataMaterial(id);
        if (res.getCount() != 0) {

            while (res.moveToNext()) {
                Sem.add(new MaterialData(res.getString(0),res.getString(1),res.getString(2)));
                //Toast.makeText(getApplicationContext(),Sem.get(0).getName(),Toast.LENGTH_LONG).show();

            }
        }

        return Sem;
    }


    @Override

    public void onBackPressed() {
        super.onBackPressed();
        //Intent i = new Intent(this, Sem)
        startActivity(new Intent(Material.this, Semester.class));
        finish();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}


